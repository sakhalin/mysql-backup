<?php
/**
 * Created by JetBrains PhpStorm.
 * User: yakovlev
 * Date: 10.12.12
 * Time: 13:49
 */

return array(
    'information_schema',
    'mysql',
    'performance_schema',
    'phpmyadmin',
);