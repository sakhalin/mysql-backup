<?php
/**
 * Created by JetBrains PhpStorm.
 * User: yakovlev
 * Date: 10.12.12
 * Time: 13:50
 */


class MysqldumpCommand extends ConsoleCommand {

    protected $bases;
    protected $backupDirToday;

    public function run($args) {
        $this->write2Console('Starting mysqldump backup');
        $this->initBases();
        $this->backupDirToday = Yii::app()->params['backupdir'] . DIRECTORY_SEPARATOR . date('Y-m-d');
        $this->write2Console( 'Backup dir: ' . $this->backupDirToday, true, true);
        if (!is_dir($this->backupDirToday)) {
            mkdir($this->backupDirToday);
        }

        /**
         * Backup bases
         */
        foreach ($this->bases as $base) {
            $this->backupWithMysqlDump($base);
        }
        $this->writeLog2File($this->backupDirToday . DIRECTORY_SEPARATOR .'!.log');
    }


    protected function initBases() {
        $connection = $this->getDB();
        $dataReader = $connection->createCommand('SHOW DATABASES')->query();
        $ignore = include(dirname(__FILE__) . '/../data/ignoreBase.php');
        foreach($dataReader as $row) {
            $base = $row['Database'];
            if (in_array($base, $ignore)) {
                continue;
            }
            $this->bases [] = $base;
        }
        return true;
    }

    /**
     * @param $basename
     */
    protected function backupWithMysqlDump($basename) {
        $this->write2Console("Backup $basename", false);
        $db = $this->getDB();
        $username = $db->username;
        $pass = ($db->password == null ? '' : '-p' . $db->password);
        $path = $this->backupDirToday . DIRECTORY_SEPARATOR . $basename . '.sql.gz';
        //exec("mysqldump -u$username $pass  --lock-tables --opt $basename > $path");
        exec("mysqldump -u$username $pass  --lock-tables --opt $basename | gzip > $path");
        $this->write2Console("...ok");
    }

}